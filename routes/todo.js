var express = require('express');
var router = express.Router();
const todosController = require('./controllers/todo.controller');
/* GET users listing. */

router.get('/', todosController.show);
router.post('/', todosController.create);
router.put('/:todolist_id', todosController.update);
router.delete('/:todolist_id', todosController.delete);
module.exports = router;

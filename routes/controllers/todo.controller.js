const Todo = require('../../models/Todo');

exports.show = async (req, res, next) => {
  try {
    const todoList = await Todo.find();
    res.render('todolist', { todos: todoList });

  } catch (err) {
    next(err);
  }
};

exports.create = async (req, res, next) => {
  try {
    const { body: { todo } } = req;
    if (!todo) throw Error('todo is not defined');

    const newTodo = await Todo({ todo });
    newTodo.save();

    res.redirect('/todolist');
  } catch (err) {
    next(err);
  }
};

exports.update = async (req, res, next) => {
  try {
    console.log(req);
    const id = req.params.todolist_id;
    if (!id) throw Error('id is not defined');
    await Todo.findByIdAndUpdate(id, { todo: 'done' });
    res.redirect('/todolist');
  } catch (err) {
    next(err);
  }
};

exports.delete = async (req, res, next) => {
  try {
    const id = req.params.todolist_id;
    if (!id) throw Error('id is not defined');
    await Todo.findByIdAndDelete(id, () => {
      console.log('deleted!!!!!!!');
    });
    res.redirect('/todolist');
  } catch (err) {
    next(err)
  }
};

const updateBtn = document.querySelectorAll('.updateBtn');
const deleteBtn = document.querySelectorAll('.deleteBtn');

updateBtn.forEach(item => {
  item.addEventListener('click', async (ev) => {
    try {
      const id = ev.target.id;
      await fetch(`http://localhost:4000/todolist/${id}`, {
        method: 'PUT'
      });
      location.assign("http://localhost:4000/todolist");
    } catch (err) {
      console.error(err);
    }
  });
});

deleteBtn.forEach(item => {
  item.addEventListener('click', async (ev) => {
    try {
      const id = ev.target.id;
      await fetch(`http://localhost:4000/todolist/${id}`, {
        method: 'DELETE'
      });
      location.assign("http://localhost:4000/todolist");
    } catch (err) {
      console.error(err);
    }
  });
});
